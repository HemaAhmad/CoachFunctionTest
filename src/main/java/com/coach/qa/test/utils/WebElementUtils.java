package com.coach.qa.test.utils;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by Hema on 12/29/2017
 */
public class WebElementUtils extends WaitUtils{

   // protected WebDriver driver;

    public WebElementUtils(WebDriver driver){
        super(driver);
    }


    public void typeText(By by, String text){ //just pass the locator and what to type.

        WebElement element = waitForElementDisplayed(by,WaitUtils.DEFAULT_WAIT_TIME); //find the element using waitForElementDisplayed method from SeleniumUtils by the constant time.
        typeText(element,text); //this line is calling typeText method below which is 3 line of code. But same method functionalty as above.
    }


    public void typeText(WebElement element, String text){ //method overload with first typeText and different parameters.

        highlight(element);
        element.clear();
        element.sendKeys(text);
    }


    //method to click a button, which is not hardcoded.
    public void click(By by){
        WebElement element = waitForElementDisplayed(by,WaitUtils.DEFAULT_WAIT_TIME);
        click(element);
    }


    //method to click a button, which is not hardcoded.Overloaded method.
    public void click(WebElement element){
        highlight(element);
        element.click();
    }




}



