package com.coach.qa.test.framework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Hema on 12/29/2017
 */
public class SmallWalletPage extends PageBase{

    private WebElement colorOption;

    private WebElement addToBagButton;

    private WebElement miniCartDisplay;

    public SmallWalletPage(WebDriver driver){ //constructor
        super(driver);
        PageFactory.initElements(driver,this);
    }
}
