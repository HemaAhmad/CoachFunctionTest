package com.coach.qa.test.framework;

import com.coach.qa.test.utils.WebElementUtils;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

/**
 * Created by Hema on 12/29/2017
 */
public class PageBase extends WebElementUtils{

    public PageBase(WebDriver driver){
        super(driver);
    }

    //This method is to verify the title of the webpage. So we can verify all page titles.
    public void verifyPageTitle(String expectedTitle){
        String title = driver.getTitle();
        Assert.assertEquals(expectedTitle,title);
    }

}
