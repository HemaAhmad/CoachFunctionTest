package com.coach.qa.test.framework;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Hema on 12/29/2017
 * HomePage class
 */
public class HomePage extends PageBase {

    @FindBy(how = How.XPATH, using = ".//*[@id='categories']/ul/li[@class='top-level gifts']")
    private WebElement giftsCategoryLink;

    @FindBy(how = How.XPATH, using = ".//*[@id='categories']//li[@class='gifts-women-under-100']")
    private WebElement forHerUnder$100Link;


    public HomePage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver,this);
    }

    public void hoverGiftsCategory() {
        giftsCategoryLink = driver.findElement(By.xpath(".//*[@id='categories']/ul/li[@class='top-level gifts']"));

        Actions actions = new Actions(driver);
        actions.moveToElement(giftsCategoryLink).perform();

        forHerUnder$100Link = driver.findElement(By.xpath(".//*[@id='categories']//li[@class='gifts-women-under-100']"));
        forHerUnder$100Link.click();

        delayFor(5000);
    }


}
