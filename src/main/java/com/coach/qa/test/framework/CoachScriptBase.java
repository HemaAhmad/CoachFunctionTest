package com.coach.qa.test.framework;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by Hema on 12/29/2017
 * ScriptBase created
 */
public class CoachScriptBase {

    protected WebDriver driver;

    protected HomePage homePage;
    protected ForHerUnder$100Page forHerUnder$100Page;
    protected SmallWalletPage smallWalletPage;


    @Before
    public void setUp(){
        //ChromeDriverManager.getInstance().setup();
        //driver = new ChromeDriver();


        //System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "/src/main/resources/drivers/geckodriver.exe");
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/src/main/resources/drivers/chromedriver.exe");
        //driver = new FirefoxDriver();
        driver = new ChromeDriver();
        //driver.manage().window().maximize();

        driver.manage().deleteAllCookies(); //used to delete cookies from site, if needed.
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(10,TimeUnit.SECONDS);

        homePage = new HomePage(driver);
        forHerUnder$100Page = new ForHerUnder$100Page(driver);
        smallWalletPage = new SmallWalletPage(driver);


        try {
            driver.navigate().to(new URL("http://coach.com")); //mean try to go to this url...
        } catch (MalformedURLException e) {  //and if cannot go to url, catch it in an exception. note: e is a variable.
            e.printStackTrace();
        } finally {
            System.out.println("Check URL format!");
        }

    }

    @After
    public void tearDown(){

        homePage = null;
        forHerUnder$100Page = null;
        smallWalletPage = null;

        driver.close();
        driver.quit();
    }


}


