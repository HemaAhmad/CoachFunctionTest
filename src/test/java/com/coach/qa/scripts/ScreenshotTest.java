package com.coach.qa.scripts;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * @author Hema on  5/20/2018
 */
public class ScreenshotTest {

    WebDriver driver;

    @BeforeClass
    public void setUp(){
        ChromeDriverManager.getInstance().setup();
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }


    @Test
    public void captureScreenshotTest1(){
        driver.navigate().to("http://spree.shiftedtech.com/");

        TakesScreenshot takesScreenshot = (TakesScreenshot) driver;
        File source = takesScreenshot.getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(source, new File("C:/MyDevelopments/IdeaProjects/CoachFunctionTest/src/main/java/com/coach/qa/test/SikuliImages/screenshot.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Screenshot Taken");
    }

    @Test
    public void captureScreenshotTest2(){
        driver.navigate().to("http://spree.shiftedtech.com/");
        takeScreenShot("OpenBrowser");

        WebElement loginLink = driver.findElement(By.xpath(".//*[@id='link-to-login']/a"));
        highlight(loginLink);
        takeScreenShot("LoginLinHighlited");
        delayFor(2000);
        loginLink.click();
        takeScreenShot("LoginPage");
    }

    public void takeScreenShot(String filename){
        TakesScreenshot takesScreenshot = (TakesScreenshot) driver;
        File source = takesScreenshot.getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(source, new File("C:/MyDevelopments/IdeaProjects/CoachFunctionTest/src/main/java/com/coach/qa/test/SikuliImages/" + filename + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void delayFor(int timeInMili){
        try {
            Thread.sleep(timeInMili);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected void highlight(WebElement element) {
        for (int i = 0; i < 2; i++) {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: 2px solid red;");
            delayFor(200);
            js.executeScript(
                    "arguments[0].setAttribute('style', arguments[1]);",
                    element, "");
            delayFor(200);
        }
    }
}
