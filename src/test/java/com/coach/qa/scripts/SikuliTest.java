package com.coach.qa.scripts;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;


/**
 * @author Hema on  5/19/2018
 * My Sikuli test.
 */
public class SikuliTest {

    @Test
    public void test1() throws FindFailed{

        Screen screen = new Screen();

        Pattern chooseFileButton = new Pattern("C:\\MyDevelopments\\IdeaProjects\\CoachFunctionTest\\src\\main\\java\\com\\coach\\qa\\test\\SikuliImages\\choosefilebutton.PNG");
        Pattern fileName = new Pattern("C:\\MyDevelopments\\IdeaProjects\\CoachFunctionTest\\src\\main\\java\\com\\coach\\qa\\test\\SikuliImages\\filename.PNG");
        Pattern openButton = new Pattern("C:\\MyDevelopments\\IdeaProjects\\CoachFunctionTest\\src\\main\\java\\com\\coach\\qa\\test\\SikuliImages\\openbutton.PNG");


        ChromeDriverManager.getInstance().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        driver.navigate().to("http://demo.automationtesting.in/Register.html");

        screen.click(chooseFileButton);
        screen.type(fileName,"C:\\Users\\Public\\Pictures\\Sample Pictures\\Penguins.jpg");
        screen.click(openButton);

    }

}
